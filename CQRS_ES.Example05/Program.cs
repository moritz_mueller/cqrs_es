﻿using CQRS_ES.Example05.Domain;
using CQRS_ES.Example05.Events;
using CQRS_ES.Example05.Persistence;
using KellermanSoftware.CompareNetObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS_ES.Example05
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount account = new BankAccount();

            List<EventBase> events = new List<EventBase>();
            account.DomainEventOccurred += domainEvent => events.Add(domainEvent);

            account.Open("Marge Simpson");

            account.Add(2000);
            account.Add(2010);
            account.Add(1000);
            account.Add(1000);
            account.Add(-2500);

            account.Close();

            IAccountWriteService writeService = new NEventStoreAccountService();
            writeService.SaveEvents(account.Id, events.AsReadOnly());

            IAccountReadService readService = new NEventStoreAccountService();
            BankAccount loadedAccount = readService.GetAccount(account.Id);

            CompareLogic compareLogic = new CompareLogic();
            var compareResult = compareLogic.Compare(account, loadedAccount);
        }
    }
}
