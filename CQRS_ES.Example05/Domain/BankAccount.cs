﻿using CQRS_ES.Example05.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS_ES.Example05.Domain
{
    public class BankAccount
    {
        #region Properties

        public Guid Id
        {
            get;
            private set;
        }

        public bool IsActive
        {
            get;
            private set;
        }

        public decimal Balance
        {
            get;
            private set;
        }

        public string Owner
        {
            get;
            private set;
        }

        #endregion

        #region Event

        public event Action<EventBase> DomainEventOccurred;

        #endregion

        #region Constructors

        public BankAccount()
        {
            this.Id = Guid.NewGuid();
        }

        #endregion

        #region Methods

        private void raiseDomainEvent(EventBase @event)
        {
            this.DomainEventOccurred?.Invoke(@event);
        }

        public void Add(decimal amount)
        {
            this.Balance += amount;

            if(amount > 0)
            {
                this.raiseDomainEvent(new Events.DepositReceived(this.Id, amount));
            }

            if(amount < 0)
            {
                this.raiseDomainEvent(new Events.WithdrawelOfCash(this.Id, 17, amount));
            }

            if(this.Balance < 0)
            {
                this.raiseDomainEvent(new Events.AccountOverdrawn(this.Id, this.Balance));
            }
        }

        public void Open(string owner, Guid id = new Guid())
        {
            if(id != Guid.Empty)
            {
                this.Id = id;
            }
            this.IsActive = true;
            this.Owner = owner;
            this.raiseDomainEvent(new BankAccountOpened(this.Id, this.Owner));
        }

        public void Close()
        {
            this.IsActive = false;
            this.raiseDomainEvent(new BankAccountClosed(this.Id));
        }

        #endregion
    }
}
