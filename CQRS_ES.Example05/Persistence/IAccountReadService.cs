﻿using CQRS_ES.Example05.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS_ES.Example05.Persistence
{
    public interface IAccountReadService
    {
        BankAccount GetAccount(Guid id);
    }
}
