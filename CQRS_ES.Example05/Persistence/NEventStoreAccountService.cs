﻿using NEventStore;
using NEventStore.Persistence.Sql.SqlDialects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CQRS_ES.Example05.Events;
using CQRS_ES.Example05.Domain;

namespace CQRS_ES.Example05.Persistence
{
    public class NEventStoreAccountService : IAccountWriteService, IAccountReadService
    {
        private IStoreEvents _connection;

        public NEventStoreAccountService()
        {
        }

        private IStoreEvents getConnection()
        {
            if (this._connection == null)
            {
                this._connection
                      = Wireup.Init()
                              .UsingSqlPersistence("NEventStoreConnection")
                              .WithDialect(new MsSqlDialect())
                              .InitializeStorageEngine()
                              .UsingJsonSerialization()
                              .Compress()
                              .LogToConsoleWindow()
                              .Build();
            }

            return this._connection;
        }

        public void SaveEvents(Guid streamId, IReadOnlyCollection<EventBase> events)
        {
            using (var stream = this.getConnection().OpenStream(streamId))
            {
                foreach (var @event in events)
                {
                    stream.Add(new EventMessage { Body = @event });
                }
                stream.CommitChanges(Guid.NewGuid());
            }
        }

        public BankAccount GetAccount(Guid id)
        {
            IDictionary<Type, Action<EventBase, BankAccount>> replayMapping = new Dictionary<Type, Action<EventBase, BankAccount>>
            {
                [typeof(BankAccountOpened)] = (e, a) => a.Open(((BankAccountOpened)e).Owner, e.BankAccountId),
                [typeof(DepositReceived)] = (e, a) => a.Add(((DepositReceived)e).Amount),
                [typeof(WithdrawelOfCash)] = (e, a) => a.Add(((WithdrawelOfCash)e).Amount),
                [typeof(BankAccountClosed)] = (e, a) => a.Close()
            };

            using (var stream = this.getConnection().OpenStream(id))
            {
                BankAccount account = new BankAccount();

                foreach (var @event in stream.CommittedEvents)
                {
                    var eventType = @event.Body.GetType();
                    if (replayMapping.ContainsKey(eventType))
                    {
                        var action = replayMapping[@event.Body.GetType()];
                        action((EventBase)@event.Body, account);
                    }
                }

                return account;
            }
        }
    }
}
