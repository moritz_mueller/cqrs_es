﻿using CQRS_ES.Example05.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS_ES.Example05.Persistence
{
    public interface IAccountWriteService
    {
        void SaveEvents(Guid streamId, IReadOnlyCollection<EventBase> @event);
    }
}
