﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS_ES.Example05.Events
{
    public abstract class EventBase
    {
        public DateTime Timestamp
        {
            get;
            set;
        }

        public Guid BankAccountId
        {
            get;
            set;
        }

        protected EventBase(Guid bankAccountId)
        {
            this.BankAccountId = bankAccountId;
            this.Timestamp = DateTime.Now;
        }
    }
}
