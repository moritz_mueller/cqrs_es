﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS_ES.Example05.Events
{
    public class WithdrawelOfCash : EventBase
    {
        public int AtmId { get; }

        public decimal Amount { get; }

        public WithdrawelOfCash(Guid accountId, int atmId, decimal amount) 
            : base(accountId)
        {
            this.AtmId = atmId;
            this.Amount = amount;
        }
    }
}
