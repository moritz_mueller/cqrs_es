﻿using NEventStore;
using NEventStore.Persistence.Sql.SqlDialects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CQRS_ES.Example06.Events;
using CQRS_ES.Example06.Domain;

namespace CQRS_ES.Example06.Persistence
{
    public class NEventStoreAccountService : IAccountWriteService, IAccountReadService
    {
        private IStoreEvents _connection;
        private IDictionary<Type, Action<EventBase, BankAccount>> _replayMapping;

        public NEventStoreAccountService()
        {
            this._replayMapping = new Dictionary<Type, Action<EventBase, BankAccount>>
            {
                [typeof(BankAccountOpened)] = (e, a) => a.Open(((BankAccountOpened)e).Owner, e.BankAccountId),
                [typeof(DepositReceived)] = (e, a) => a.Add(((DepositReceived)e).Amount),
                [typeof(WithdrawelOfCash)] = (e, a) => a.Add(((WithdrawelOfCash)e).Amount),
                [typeof(BankAccountClosed)] = (e, a) => a.Close()
            };
        }

        private IStoreEvents getConnection()
        {
            if (this._connection == null)
            {
                this._connection
                      = Wireup.Init()
                              .UsingSqlPersistence("NEventStoreConnection")
                              .WithDialect(new MsSqlDialect())
                              .InitializeStorageEngine()
                              .UsingJsonSerialization()
                              .Compress()
                              .LogToConsoleWindow()
                              .Build();
            }

            return this._connection;
        }

        public void SaveEvents(Guid id, IReadOnlyCollection<EventBase> events)
        {            
            using (var stream = this.getConnection().OpenStream(id))
            {
                foreach (var @event in events)
                {
                    stream.Add(new EventMessage { Body = @event });
                    
                }
                stream.CommitChanges(Guid.NewGuid());
            }            
        }

        public void SaveEventsWithSnapshot(Guid id, IReadOnlyCollection<EventBase> @events, BankAccount account)
        {
            var connection = this.getConnection();
            using (var stream = connection.OpenStream(id))
            {
                foreach (var @event in events)
                {
                    stream.Add(new EventMessage { Body = @event });

                }
                stream.CommitChanges(Guid.NewGuid());

                Snapshot snaphot = new Snapshot(stream.StreamId, stream.StreamRevision, account);
                connection.Advanced.AddSnapshot(snaphot);
            }
        }

        public BankAccount GetAccount(Guid id)
        {
            using (var stream = this.getConnection().OpenStream(id))
            {
                BankAccount account = new BankAccount();

                foreach (var @event in stream.CommittedEvents)
                {
                    var eventType = @event.Body.GetType();
                    if (this._replayMapping.ContainsKey(eventType))
                    {
                        var action = this._replayMapping[@event.Body.GetType()];
                        action((EventBase)@event.Body, account);
                    }
                }

                return account;
            }
        }

        public BankAccount GetAccountBySnapshot(Guid id)
        {
            var connection = this.getConnection();
            var snapshot = connection.Advanced.GetSnapshot(id, int.MaxValue);
            BankAccount account = snapshot.Payload as BankAccount;

            using (var stream = connection.OpenStream(snapshot, int.MaxValue))
            {
                foreach (var @event in stream.CommittedEvents)
                {
                    var eventType = @event.Body.GetType();
                    if (this._replayMapping.ContainsKey(eventType))
                    {
                        var action = this._replayMapping[@event.Body.GetType()];
                        action((EventBase)@event.Body, account);
                    }
                }

                return account;
            }
        }
    }
}
