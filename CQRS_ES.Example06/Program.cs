﻿using CQRS_ES.Example06.Domain;
using CQRS_ES.Example06.Events;
using CQRS_ES.Example06.Persistence;
using KellermanSoftware.CompareNetObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS_ES.Example06
{
    class Program
    {
        static Func<IList<EventBase>> eventListFunc;


        static void Main(string[] args)
        {
            BankAccount account = new BankAccount();

            #region First event stream

            List<EventBase> firstEventStream = new List<EventBase>();
            eventListFunc = () => firstEventStream;
            account.DomainEventOccurred += e => eventListFunc().Add(e);

            account.Open("Marge Simpson");
            account.Add(2000);
            account.Add(-900);
            account.Add(-1000);
            account.Add(-200);
            account.Add(500);            

            IAccountWriteService writeService = new NEventStoreAccountService();
            writeService.SaveEvents(account.Id, firstEventStream.AsReadOnly());

            #endregion

            #region Second event stream

            List<EventBase> secondEventStream = new List<EventBase>();
            eventListFunc = () => secondEventStream;

            account.Add(1000);
            account.Add(2000);
            account.Add(-500);
            account.Add(-750);
            writeService.SaveEventsWithSnapshot(account.Id, secondEventStream.AsReadOnly(), account);

            #endregion

            #region Third event stream

            List<EventBase> thirdEventStream = new List<EventBase>();
            eventListFunc = () => thirdEventStream;

            account.Add(17);
            writeService.SaveEvents(account.Id, thirdEventStream.AsReadOnly());

            #endregion


            IAccountReadService readService = new NEventStoreAccountService();
            BankAccount accountLoadedByEvents = readService.GetAccount(account.Id);
            BankAccount accountLoadedBySnapshotAndEvents = readService.GetAccountBySnapshot(account.Id);
            

            CompareLogic compareLogic = new CompareLogic();
            var compareResult01 = compareLogic.Compare(account, accountLoadedByEvents);
            var compareResult02 = compareLogic.Compare(account, accountLoadedBySnapshotAndEvents);
            var compareResult03 = compareLogic.Compare(accountLoadedByEvents, accountLoadedBySnapshotAndEvents);
        }
    }
}
