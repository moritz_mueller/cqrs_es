﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS_ES.Example06.Events
{
    public class BankAccountClosed : EventBase
    {
        public BankAccountClosed(Guid accountId)
            : base(accountId)
        {

        }
    }
}
