﻿using CQRS_ES.Example08.Domain;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS_ES.Example08.Publisher
{
    class SqlPublisher
    {
        public void AddAccount(BankAccount account)
        {
            using (var connection = new SqlConnection(@"Server=.\SqlExpress;Database=CQRS_ES.Example01;Trusted_Connection=True;"))
            {
                connection.Open();

                string addQuery
                    = @"INSERT INTO 
                            [CQRS_ES.Example05].[dbo].[Account] ([Id],[Owner],[IsOpen],[Balance])
                        VALUES 
                            (@id,@owner,@isOpen,@balance)";

                using (var command = new SqlCommand(addQuery, connection))
                {
                    command.Parameters.AddWithValue("id", account.Id);
                    command.Parameters.AddWithValue("owner", account.Owner);
                    command.Parameters.AddWithValue("isOpen", account.IsActive);
                    command.Parameters.AddWithValue("Balance", account.Balance);
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
