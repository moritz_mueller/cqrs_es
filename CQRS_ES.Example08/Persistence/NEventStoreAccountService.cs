﻿using NEventStore;
using NEventStore.Persistence.Sql.SqlDialects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CQRS_ES.Example08.Events;
using CQRS_ES.Example08.Domain;
using CQRS_ES.Example08.Persistence;
using System.Transactions;
using System.Data.SqlClient;

namespace CQRS_ES.Example08.Persistence
{
    public class NEventStoreAccountService : IAccountWriteService, IAccountReadService
    {
        private IStoreEvents _connection;
        private IDictionary<Type, Action<EventBase, BankAccount>> _replayMapping;

        public NEventStoreAccountService()
        {
            this._replayMapping = new Dictionary<Type, Action<EventBase, BankAccount>>
            {
                [typeof(BankAccountOpened)] = (e, a) => a.Open(((BankAccountOpened)e).Owner, e.BankAccountId),
                [typeof(DepositReceived)] = (e, a) => a.Add(((DepositReceived)e).Amount),
                [typeof(WithdrawelOfCash)] = (e, a) => a.Add(((WithdrawelOfCash)e).Amount),
                [typeof(BankAccountClosed)] = (e, a) => a.Close()
            };
        }

        private IStoreEvents getConnection()
        {
            if (this._connection == null)
            {
                this._connection
                      = Wireup.Init()
                              .UsingSqlPersistence("NEventStoreConnection")
                              .WithDialect(new MsSqlDialect())
                              .EnlistInAmbientTransaction()
                              .InitializeStorageEngine()
                              .UsingJsonSerialization()
                              .Compress()
                              .LogToConsoleWindow()
                              .Build();
            }

            return this._connection;
        }

        public void SaveEventsAndPublish(BankAccount account, IReadOnlyCollection<EventBase> events)
        {
            using (var scope = new TransactionScope())
            {                
                using (var stream = this.getConnection().OpenStream(account.Id))
                {
                    #region Save events

                    foreach (var @event in events)
                    {
                        stream.Add(new EventMessage { Body = @event });

                    }
                    stream.CommitChanges(Guid.NewGuid());

                    #endregion

                    #region Publish bank account

                    using (var connection = new SqlConnection(@"Server=.\SqlExpress;Database=CQRS_ES.Example01;Trusted_Connection=True;"))
                    {
                        connection.Open();

                        string addQuery
                            = @"INSERT INTO 
                                    [CQRS_ES.Example05].[dbo].[Account] ([Id],[Owner],[IsOpen],[Balance])
                                VALUES 
                                    (@id,@owner,@isOpen,@balance)";

                        using (var command = new SqlCommand(addQuery, connection))
                        {
                            command.Parameters.AddWithValue("id", account.Id);
                            command.Parameters.AddWithValue("owner", account.Owner);
                            command.Parameters.AddWithValue("isOpen", account.IsActive);
                            command.Parameters.AddWithValue("Balance", account.Balance);
                            command.ExecuteNonQuery();

                            scope.Complete();
                        }
                    }

                    #endregion
                }
            }          
        }

        public BankAccount GetAccount(Guid id)
        {
            using (var stream = this.getConnection().OpenStream(id))
            {
                BankAccount account = new BankAccount();

                foreach (var @event in stream.CommittedEvents)
                {
                    var eventType = @event.Body.GetType();
                    if (this._replayMapping.ContainsKey(eventType))
                    {
                        var action = this._replayMapping[@event.Body.GetType()];
                        action((EventBase)@event.Body, account);
                    }
                }

                return account;
            }
        }

        public BankAccount GetAccountBySnapshot(Guid id)
        {
            var connection = this.getConnection();
            var snapshot = connection.Advanced.GetSnapshot(id, int.MaxValue);

            using (var stream = connection.OpenStream(snapshot, int.MaxValue))
            {
                BankAccount account = snapshot.Payload as BankAccount;

                foreach (var @event in stream.CommittedEvents)
                {
                    var eventType = @event.Body.GetType();
                    if (this._replayMapping.ContainsKey(eventType))
                    {
                        var action = this._replayMapping[@event.Body.GetType()];
                        action((EventBase)@event.Body, account);
                    }
                }

                return account;
            }
        }

        public IReadOnlyCollection<EventBase> GetEvents(Guid id)
        {
            using (var stream = this.getConnection().OpenStream(id))
            {
                return stream.CommittedEvents.Select(e => e.Body).Cast<EventBase>().ToList();
            }
        }
    }
}
