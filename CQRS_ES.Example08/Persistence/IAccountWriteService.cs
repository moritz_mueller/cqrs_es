﻿using CQRS_ES.Example08.Domain;
using CQRS_ES.Example08.Events;
using NEventStore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS_ES.Example08.Persistence
{
    public interface IAccountWriteService
    {
        void SaveEventsAndPublish(BankAccount account, IReadOnlyCollection<EventBase> @event);
    }
}
