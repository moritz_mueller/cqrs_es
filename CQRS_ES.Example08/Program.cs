﻿using CQRS_ES.Example08.Domain;
using CQRS_ES.Example08.Events;
using CQRS_ES.Example08.Persistence;
using CQRS_ES.Example08.Events;
using CQRS_ES.Example08.Persistence;
using KellermanSoftware.CompareNetObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS_ES.Example08
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount account = new BankAccount();

            List<EventBase> eventStream = new List<EventBase>();            
            account.DomainEventOccurred += e =>eventStream.Add(e);

            account.Open("Marge Simpson");
            account.Add(2000);
            account.Add(-2050);
            account.Add(3000);
            account.Add(-4000);            

            IAccountWriteService writeService = new NEventStoreAccountService();
            writeService.SaveEventsAndPublish(account, eventStream.AsReadOnly());
        }
    }
}
