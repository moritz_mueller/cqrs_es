﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS_ES.Example08.Events
{
    public class WithdrawelOfCash : EventBase
    {
        public int AtmId { get; set; }

        public decimal Amount { get; set; }

        public WithdrawelOfCash(Guid accountId, int atmId, decimal amount) 
            : base(accountId)
        {
            this.AtmId = atmId;
            this.Amount = amount;
        }
    }
}
