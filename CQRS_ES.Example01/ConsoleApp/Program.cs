﻿using Newtonsoft.Json;
using PersistenceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            AddPrices();
            //GetAll();

            Console.ReadLine();
        }

        public static void AddPrices()
        {
            PersistenceLayer.Repository repo = new PersistenceLayer.Repository();

            Accommodation accommodation01 = new Accommodation();
            accommodation01.Name = "Residence Les Chalets du Thabor";
            accommodation01.Prices.Add(new Price() { PurchasePrice = 200, SellingPrice = 250, ArrivalDate = DateTime.Parse("02/01/16") });
            accommodation01.Prices.Add(new Price() { PurchasePrice = 100, SellingPrice = 125, ArrivalDate = DateTime.Parse("09/01/16") });
            accommodation01.Prices.Add(new Price() { PurchasePrice = 150, SellingPrice = 180, ArrivalDate = DateTime.Parse("16/01/16") });

            Accommodation accommodation02 = new Accommodation();
            accommodation02.Name = "Hotel Olympia";
            accommodation02.Prices.Add(new Price() { PurchasePrice = 350, SellingPrice = 425, ArrivalDate = DateTime.Parse("02/01/16") });
            accommodation02.Prices.Add(new Price() { PurchasePrice = 300, SellingPrice = 350, ArrivalDate = DateTime.Parse("07/01/16") });
            accommodation02.Prices.Add(new Price() { PurchasePrice = 320, SellingPrice = 380, ArrivalDate = DateTime.Parse("13/01/16") });

            Accommodation accommodation03 = new Accommodation();
            accommodation03.Name = "Les Chalets de SuperDévoluy";
            accommodation03.Prices.Add(new Price() { PurchasePrice = 180, SellingPrice = 200, ArrivalDate = DateTime.Parse("02/01/16") });
            accommodation03.Prices.Add(new Price() { PurchasePrice = 90, SellingPrice = 120, ArrivalDate = DateTime.Parse("09/01/16") });
            accommodation03.Prices.Add(new Price() { PurchasePrice = 90, SellingPrice = 130, ArrivalDate = DateTime.Parse("18/01/16") });

            repo.Save(accommodation01,accommodation02,accommodation03);
        }

        public static void GetAll()
        {
            PersistenceLayer.Repository repo = new PersistenceLayer.Repository();

            foreach (var accommodation in repo.All())
            {
                Console.WriteLine("Name: {0}", accommodation.Name);
                foreach (var price in accommodation.Prices)
                {
                    Console.WriteLine("\tDate: {0} | Purchase price: {1} | Selling price: {2}", price.ArrivalDate, price.PurchasePrice, price.SellingPrice);
                }
            }
        }
    }
}
