﻿using FluentNHibernate.Mapping;

namespace PersistenceLayer
{
    class AccommodationMapping : ClassMap<Accommodation>
    {
        public AccommodationMapping()
        {
            this.Id(e => e.Id);
            this.Map(e => e.Name);
            this.HasMany<Price>(e => e.Prices).KeyColumn("AccommodationId").Not.KeyNullable()
                                              .Cascade.AllDeleteOrphan().Not.LazyLoad();
        }
    }
}
