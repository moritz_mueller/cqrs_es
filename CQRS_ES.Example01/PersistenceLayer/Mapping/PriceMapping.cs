﻿using FluentNHibernate.Mapping;

namespace PersistenceLayer
{
    class PriceMapping : ClassMap<Price>
    {
        public PriceMapping()
        {
            this.Id(e => e.Id);
            this.Map(e => e.PurchasePrice);
            this.Map(e => e.SellingPrice);
            this.Map(e => e.ArrivalDate);
        }
    }
}
