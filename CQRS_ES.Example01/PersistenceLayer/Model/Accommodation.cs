﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenceLayer
{
    /// <summary>
    /// Represents an accommodation, e.g. a "Skihütte".
    /// </summary>
    public class Accommodation
    {
        /// <summary>
        /// The internal id of the accommodation
        /// </summary>
        public virtual int Id
        {
            get;
            protected set;
        }

        /// <summary>
        /// The readable name
        /// </summary>
        public virtual string Name
        {
            get;
            set;
        }

        /// <summary>
        /// A list of prices specific for an arrival day
        /// </summary>
        public virtual IList<Price> Prices
        {
            get;
            set;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public Accommodation()
        {
            this.Prices = new List<Price>();
        }
    }
}
