﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenceLayer
{
    /// <summary>
    /// Provides access to the persistence layer
    /// </summary>
    public class Repository
    {
        /// <summary>
        /// Creates a NH session factory
        /// </summary>
        /// <returns></returns>
        private ISessionFactory createSessionFactory()
        {
            return Fluently.Configure()
                           .Database(MsSqlConfiguration.MsSql2012.ConnectionString(@"Server=.\SqlExpress;Database=CQRS_ES.Example01;Trusted_Connection=True;").ShowSql().AdoNetBatchSize(0))
                           .Mappings(m => m.FluentMappings.AddFromAssemblyOf<Accommodation>())
                           .Diagnostics(a => a.OutputToConsole())                           
                           .BuildSessionFactory();
        }

        /// <summary>
        /// Executes an arbitrary action within a NH session and a SQL transaction
        /// </summary>
        /// <param name="queryAction"></param>
        private void executeQuery(Action<ISession> queryAction)
        {
            using (var sessionFactory = this.createSessionFactory())
            {
                using (var session = sessionFactory.OpenSession())
                {
                    using(var transaction = session.BeginTransaction())
                    {
                        queryAction(session);
                        transaction.Commit();
                    }                    
                }
            }
        }

        /// <summary>
        /// Persists all given accommodations or none
        /// </summary>
        /// <param name="accommodations"></param>
        public void Save(params Accommodation[] accommodations)
        {
            this.executeQuery(session => 
            {
                foreach (var item in accommodations)
                {
                    session.Save(item);                    
                }                
            });
        }

        /// <summary>
        /// Returns all accommodations including price data
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Accommodation> All()
        {
            IEnumerable<Accommodation> accommodations = new List<Accommodation>();

            this.executeQuery(session =>
            {
                IList<Accommodation> entities = session.QueryOver<Accommodation>().List();
                //foreach (var accommodation in entities)
                //{
                //    accommodation.Prices
                //                 .ToList()
                //                 .ForEach(p => p.PurchasePrice = 0);
                //}                

                accommodations = entities;
            });

            return accommodations;
        }
    }
}
