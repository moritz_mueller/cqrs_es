﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenceLayer
{
    public class Accommodation
    {
        public virtual int Id
        {
            get;
            protected set;
        }

        public virtual string Name
        {
            get;
            set;
        }

        public virtual IList<Price> Prices
        {
            get;
            set;
        }

        public Accommodation()
        {
            this.Prices = new List<Price>();
        }
    }
}
