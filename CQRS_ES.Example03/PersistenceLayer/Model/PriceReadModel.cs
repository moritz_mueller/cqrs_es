﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenceLayer
{
    /// <summary>
    /// Contains price information for a specific arrival date
    /// </summary>
    public class PriceReadModel
    {
        public virtual int Id
        {
            get;
            set;
        }


        /// <summary>
        /// The sellingh price for one person on the specific arrival date
        /// </summary>
        public virtual decimal SellingPrice
        {
            get;
            set;
        }

        /// <summary>
        /// The arrival date the prices are valid for
        /// </summary>
        public virtual DateTime ArrivalDate
        {
            get;
            set;
        }
    }
}
