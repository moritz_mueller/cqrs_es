﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenceLayer
{
    class AccommodationMapping : ClassMap<Accommodation>
    {
        public AccommodationMapping()
        {
            this.Id(e => e.Id);
            this.Map(e => e.Name);
            this.HasMany<Price>(e => e.Prices).KeyColumn("AccommodationId").Not.KeyNullable()
                                              .Cascade.AllDeleteOrphan();
        }
    }
}
