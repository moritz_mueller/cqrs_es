﻿using FluentNHibernate.Mapping;

namespace PersistenceLayer
{
    class AccommodationWriteModelMapping : ClassMap<AccommodationWriteModel>
    {
        public AccommodationWriteModelMapping()
        {
            this.Id(e => e.Id);
            this.Map(e => e.Name);
            this.HasMany<PriceWriteModel>(e => e.Prices).KeyColumn("AccommodationId").Not.KeyNullable()
                                              .Cascade.AllDeleteOrphan();
            this.Table("Accommodation");
        }
    }
}
