﻿using CQRS_ES.Example07.Domain;
using CQRS_ES.Example07.Events;
using CQRS_ES.Example07.Persistence;
using CQRS_ES.Example07.Events;
using CQRS_ES.Example07.Persistence;
using KellermanSoftware.CompareNetObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS_ES.Example07
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Initial event stream

            BankAccount account = new BankAccount();

            List<EventBase> eventStream = new List<EventBase>();            
            account.DomainEventOccurred += e =>eventStream.Add(e);

            account.Open("Marge Simpson");
            account.Add(2000);
            account.Add(-2050);
            account.Add(3000);
            account.Add(-4000);            

            IAccountWriteService writeService = new NEventStoreAccountService();
            writeService.SaveEvents(account.Id, eventStream.AsReadOnly());

            #endregion

            IAccountReadService readService = new NEventStoreAccountService();
            var bankAccountEvents = readService.GetEvents(account.Id);

            IList<EventBase> newEventStream = new List<EventBase>();

            foreach (var @event in bankAccountEvents)
            {
                newEventStream.Add(@event);

                AccountOverdrawn accountOverdrawnEvent = @event as AccountOverdrawn;
                if(accountOverdrawnEvent != null)
                {
                    if(accountOverdrawnEvent.AmountDue > 50)
                    {
                        FeeDemanded feeEvent = new FeeDemanded(account.Id);
                        feeEvent.Amount = accountOverdrawnEvent.AmountDue * 0.2m;
                        newEventStream.Add(feeEvent);
                    }
                }
            }

            decimal amountFees = newEventStream.Where(e => e is FeeDemanded)
                                               .Sum(e => ((FeeDemanded)e).Amount);
        }
    }
}
