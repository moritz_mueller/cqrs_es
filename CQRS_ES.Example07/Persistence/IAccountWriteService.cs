﻿using CQRS_ES.Example07.Domain;
using CQRS_ES.Example07.Events;
using NEventStore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS_ES.Example07.Persistence
{
    public interface IAccountWriteService
    {
        void SaveEvents(Guid id, IReadOnlyCollection<EventBase> @event);

        void SaveEventsWithSnapshot(Guid id, IReadOnlyCollection<EventBase> @event, BankAccount account);
    }
}
