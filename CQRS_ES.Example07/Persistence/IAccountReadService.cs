﻿using CQRS_ES.Example07.Domain;
using CQRS_ES.Example07.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS_ES.Example07.Persistence
{
    public interface IAccountReadService
    {
        BankAccount GetAccount(Guid id);

        BankAccount GetAccountBySnapshot(Guid id);

        IReadOnlyCollection<EventBase> GetEvents(Guid id);
    }
}
