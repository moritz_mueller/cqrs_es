﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS_ES.Example07.Events
{
    public class AccountOverdrawn : EventBase
    {
        public decimal AmountDue { get; set; }

        public AccountOverdrawn(Guid accountId, decimal amoundDue)
            : base(accountId)
        {
            this.AmountDue = amoundDue;
        }
    }
}
