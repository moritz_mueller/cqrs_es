﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS_ES.Example07.Events
{
    public class DepositReceived : EventBase
    {
        public decimal Amount { get; set; }

        public DepositReceived(Guid accountId, decimal amount)
            : base(accountId)
        {
            this.Amount = amount;
        }
    }
}
