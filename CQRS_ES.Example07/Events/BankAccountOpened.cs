﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS_ES.Example07.Events
{
    public class BankAccountOpened : EventBase
    {
        public string Owner { get; set; }

        public BankAccountOpened(Guid accountId, string owner)
            : base(accountId)
        {
            this.Owner = owner;
        }
    }
}
