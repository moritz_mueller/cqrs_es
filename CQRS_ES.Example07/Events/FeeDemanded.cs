﻿using CQRS_ES.Example07.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS_ES.Example07.Events
{
    public class FeeDemanded : EventBase
    {
        public decimal Amount { get; set; }

        public FeeDemanded(Guid bankAccountId) : base(bankAccountId)
        {
        }       

    }
}
