﻿using CommandStack.ServiceBus.Events;
using NServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class ProgressReportedHandler : IHandleMessages<ProgressReportedEvent>
    {
        public void Handle(ProgressReportedEvent message)
        {
            Console.WriteLine(message.Description);
        }
    }
}
