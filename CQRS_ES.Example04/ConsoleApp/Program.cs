﻿using CommandStack.ServiceBus.Commands;
using Newtonsoft.Json;
using NServiceBus;
using PersistenceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            BusConfiguration busConfiguration = new BusConfiguration();
            busConfiguration.EndpointName("Accommodation.Contracts.Client");
            busConfiguration.UseSerialization<NServiceBus.JsonSerializer>();
            busConfiguration.EnableInstallers();
            busConfiguration.UsePersistence<InMemoryPersistence>();

            using (IBus bus = Bus.Create(busConfiguration).Start())
            {
                AddPrices(bus);
                Console.ReadLine();
            }            
        }

        public static void AddPrices(IBus bus)
        {
            AccommodationWriteModel accommodation01 = new AccommodationWriteModel();
            accommodation01.Name = "Residence Les Chalets du Thabor";
            accommodation01.Prices.Add(new PriceWriteModel() { PurchasePrice = 200, SellingPrice = 250, ArrivalDate = DateTime.Parse("02/01/16") });
            accommodation01.Prices.Add(new PriceWriteModel() { PurchasePrice = 100, SellingPrice = 125, ArrivalDate = DateTime.Parse("09/01/16") });
            accommodation01.Prices.Add(new PriceWriteModel() { PurchasePrice = 150, SellingPrice = 180, ArrivalDate = DateTime.Parse("16/01/16") });

            bus.Send("Accommodation.Contracts.Server", new AddAccommodationCommand() { Data = accommodation01 });
        }
    }
}
