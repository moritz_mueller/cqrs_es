﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenceLayer
{
    /// <summary>
    /// Represents an accommodation, e.g. a "Skihütte".
    /// </summary>
    public class AccommodationWriteModel
    {
        /// <summary>
        /// The internal id of the accommodation
        /// </summary>
        public virtual int Id
        {
            get;
            set;
        }

        /// <summary>
        /// The readable name
        /// </summary>
        public virtual string Name
        {
            get;
            set;
        }

        /// <summary>
        /// A list of prices specific for an arrival day
        /// </summary>
        public virtual IList<PriceWriteModel> Prices
        {
            get;
            set;
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public AccommodationWriteModel()
        {
            this.Prices = new List<PriceWriteModel>();
        }
    }
}
