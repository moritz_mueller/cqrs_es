﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenceLayer
{
    /// <summary>
    /// Contains price information for a specific arrival date
    /// </summary>
    public class PriceWriteModel
    {
        /// <summary>
        /// The internal id of this price row
        /// </summary>
        public virtual int Id
        {
            get;
            set;
        }

        /// <summary>
        /// The purchase price for one person on the specific arrival date
        /// </summary>
        public virtual decimal PurchasePrice
        {
            get;
            set;
        }

        /// <summary>
        /// The sellingh price for one person on the specific arrival date
        /// </summary>
        public virtual decimal SellingPrice
        {
            get;
            set;
        }

        /// <summary>
        /// The arrival date the prices are valid for
        /// </summary>
        public virtual DateTime ArrivalDate
        {
            get;
            set;
        }
    }
}
