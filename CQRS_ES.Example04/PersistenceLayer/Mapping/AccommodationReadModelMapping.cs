﻿using FluentNHibernate.Mapping;

namespace PersistenceLayer
{
    class AccommodationReadModelMapping : ClassMap<AccommodationReadModel>
    {
        public AccommodationReadModelMapping()
        {
            this.Id(e => e.Id);
            this.Map(e => e.Name);
            this.HasMany<PriceReadModel>(e => e.Prices).KeyColumn("AccommodationId").Not.KeyNullable()
                                              .Cascade.AllDeleteOrphan().Not.LazyLoad();
            this.Table("Accommodation");
        }
    }
}
