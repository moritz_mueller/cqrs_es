﻿using FluentNHibernate.Mapping;

namespace PersistenceLayer
{
    class PriceWriteModelMapping : ClassMap<PriceWriteModel>
    {
        public PriceWriteModelMapping()
        {
            this.Id(e => e.Id);
            this.Map(e => e.PurchasePrice);
            this.Map(e => e.SellingPrice);
            this.Map(e => e.ArrivalDate);
            this.Table("Price");
        }
    }
}
