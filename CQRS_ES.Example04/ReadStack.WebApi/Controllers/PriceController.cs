﻿using Newtonsoft.Json;
using PersistenceLayer;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ReadStack.WebApi.Controllers
{
    public class PriceController : ApiController
    {
        /// <summary>
        /// Returns the prices for the accommodation with the given id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<HttpResponseMessage> Get(int id)
        {
            using (ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("localhost"))
            {
                IDatabase connection = redis.GetDatabase();
                string cachedJsonObject = await connection.StringGetAsync(id.ToString());

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(cachedJsonObject, System.Text.Encoding.UTF8, "application/json");
                return response;
            }
        }

        /// <summary>
        /// Adds the price data for the given accommodation
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<HttpResponseMessage> Add(AccommodationReadModel accommodation)
        {
            using (ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("localhost"))
            {
                IDatabase connection = redis.GetDatabase();
                string content = await this.Request.Content.ReadAsStringAsync();
                await connection.StringSetAsync(accommodation.Id.ToString(), JsonConvert.SerializeObject(accommodation.Prices));
                return this.Request.CreateResponse(HttpStatusCode.Accepted);
            }
        }
    }
}
