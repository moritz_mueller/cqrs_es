﻿using CommandStack.ServiceBus.Commands;
using CommandStack.ServiceBus.Events;
using NServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CommandStack.ServiceBus.Handlers
{
    class PublishAccommodationHandler : IHandleMessages<PublishAccommodationCommand>
    {
        private readonly IBus bus;

        public PublishAccommodationHandler(IBus bus)
        {
            this.bus = bus;
        }

        public void Handle(PublishAccommodationCommand message)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri("http://localhost:2579/api/Price/");
                var response = httpClient.PostAsJsonAsync("Add", message.Data).Result;
                response.EnsureSuccessStatusCode();
                this.bus.Reply(new AccommodationPublishedEvent() { Data = message.Data });
            }
        }
    }
}
