﻿using CommandStack.ServiceBus.Commands;
using CommandStack.ServiceBus.Events;
using NServiceBus;
using System.Linq;

namespace CommandStack.ServiceBus.Handlers
{
    class VerifyAccommodationHandler : IHandleMessages<VerifyAccommodationCommand>
    {
        private readonly IBus bus;

        public VerifyAccommodationHandler(IBus bus)
        {
            this.bus = bus;
        }


        public void Handle(VerifyAccommodationCommand message)
        {
            //Simple validation example
            AccommodationVerifiedEvent @event = new AccommodationVerifiedEvent() { Data = message.Data };
            @event.IsValid = message.Data.Prices.All(p => p.SellingPrice > p.PurchasePrice);
            this.bus.Reply(@event);
        }
    }
}
