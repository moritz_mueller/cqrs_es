﻿using CommandStack.ServiceBus.Commands;
using CommandStack.ServiceBus.Events;
using NServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandStack.ServiceBus.Handlers
{
    class PersistAccommodationHandler : IHandleMessages<PersistAccommodationCommand>
    {
        private readonly IBus bus;

        public PersistAccommodationHandler(IBus bus)
        {
            this.bus = bus;
        }

        public void Handle(PersistAccommodationCommand message)
        {
            //Persist the accommodation in the sql database
            PersistenceLayer.Repository repo = new PersistenceLayer.Repository();
            repo.Save(message.Data);
            this.bus.Reply(new AccommodationPersistedEvent() { Data = message.Data });
        }
    }
}
