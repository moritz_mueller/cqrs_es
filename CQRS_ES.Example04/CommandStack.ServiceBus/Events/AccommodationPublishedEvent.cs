﻿using NServiceBus;
using PersistenceLayer;

namespace CommandStack.ServiceBus.Events
{
    class AccommodationPublishedEvent : IMessage
    {
        public AccommodationWriteModel Data
        {
            get;
            set;
        }
    }
}
