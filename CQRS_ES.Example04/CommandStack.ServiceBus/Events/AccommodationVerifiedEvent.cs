﻿using NServiceBus;
using PersistenceLayer;
using System;

namespace CommandStack.ServiceBus.Events
{
    class AccommodationVerifiedEvent : IMessage
    {
        public AccommodationWriteModel Data
        {
            get;
            set;
        }

        public Boolean IsValid
        {
            get;
            set;
        }
    }
}
