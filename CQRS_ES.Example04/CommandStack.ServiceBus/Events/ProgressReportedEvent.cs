﻿using NServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandStack.ServiceBus.Events
{
    public class ProgressReportedEvent : IEvent
    {
        public string Description { get; set; }
    }
}
