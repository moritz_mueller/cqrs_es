﻿using NServiceBus;
using PersistenceLayer;

namespace CommandStack.ServiceBus.Events
{
    class AccommodationPersistedEvent : IMessage
    {
        public AccommodationWriteModel Data
        {
            get;
            set;
        }
    }
}
