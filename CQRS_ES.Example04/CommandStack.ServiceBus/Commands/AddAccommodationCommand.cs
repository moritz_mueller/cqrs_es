﻿using System;
using NServiceBus;
using NServiceBus.Saga;
using PersistenceLayer;

namespace CommandStack.ServiceBus.Commands
{
    public class AddAccommodationCommand : ICommand
    {
        public AccommodationWriteModel Data
        {
            get;
            set;
        }
    }
}
