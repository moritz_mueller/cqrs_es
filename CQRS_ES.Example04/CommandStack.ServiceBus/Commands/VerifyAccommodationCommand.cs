﻿using NServiceBus;
using PersistenceLayer;

namespace CommandStack.ServiceBus.Commands
{
    class VerifyAccommodationCommand : ICommand
    {
        public AccommodationWriteModel Data
        {
            get;
            set;
        }        
    }
}
