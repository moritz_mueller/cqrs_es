﻿using NServiceBus;
using PersistenceLayer;

namespace CommandStack.ServiceBus.Commands
{
    class PersistAccommodationCommand : ICommand
    {
        public AccommodationWriteModel Data
        {
            get;
            set;
        }
    }
}
