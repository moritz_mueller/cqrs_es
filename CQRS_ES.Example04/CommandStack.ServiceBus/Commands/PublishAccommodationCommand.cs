﻿using NServiceBus;
using PersistenceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandStack.ServiceBus.Commands
{
    class PublishAccommodationCommand : ICommand
    {
        public AccommodationWriteModel Data
        {
            get;
            set;
        }
    }
}
