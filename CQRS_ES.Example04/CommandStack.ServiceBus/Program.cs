﻿using NServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandStack.ServiceBus
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Bus config

            BusConfiguration busConfiguration = new BusConfiguration();
            busConfiguration.EndpointName("Accommodation.Contracts.Server");
            busConfiguration.UseSerialization<JsonSerializer>();
            busConfiguration.EnableInstallers();
            busConfiguration.UsePersistence<InMemoryPersistence>();

            #endregion

            using (IBus bus = Bus.Create(busConfiguration).Start())
            {
                Console.WriteLine("Started NServiceBus");
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
            }
        }
    }
}
