﻿using NServiceBus.Saga;
using CommandStack.ServiceBus.Commands;
using NServiceBus;
using CommandStack.ServiceBus.Events;
using System;
using PersistenceLayer;

namespace CommandStack.ServiceBus.Sagas
{
    class AddAccommodationSaga : IAmStartedByMessages<AddAccommodationCommand>
        , IHandleMessages<AccommodationVerifiedEvent>
        , IHandleMessages<AccommodationPersistedEvent>
        , IHandleMessages<AccommodationPublishedEvent>
    {
        private readonly IBus bus;

        public AddAccommodationSaga(IBus bus)
        {
            this.bus = bus;
        }

        public void Handle(AddAccommodationCommand message)
        {
            //Business process started...

            //Verify accommodation data according to business rules
            this.bus.SendLocal(new VerifyAccommodationCommand() { Data = message.Data });
            this.bus.Publish<ProgressReportedEvent>(p => p.Description = "Received AddAccommodationCommand");
        }

        public void Handle(AccommodationVerifiedEvent message)
        {
            //Accommodation verification has been completed. Check the result
            //in order to determine the next action
            if(message.IsValid)
            {
                this.bus.SendLocal(new PersistAccommodationCommand() { Data = message.Data });
                this.bus.Publish<ProgressReportedEvent>(p => p.Description = string.Format("Accommodation {0} has been succesfully validated", message.Data.Name));
            }
            else
            {
                this.bus.Publish<ProgressReportedEvent>(p => p.Description = string.Format("Accommodation {0} could not be successfully validated", message.Data.Name));
            }
        }

        public void Handle(AccommodationPersistedEvent message)
        {
            //The accommodation data have been persisted. Now it is time to update
            //the read only database
            this.bus.SendLocal(new PublishAccommodationCommand() { Data = message.Data });
            this.bus.Publish<ProgressReportedEvent>(p => p.Description = string.Format("Accommodation {0} has been succesfully persisted", message.Data.Name));
        }

        public void Handle(AccommodationPublishedEvent message)
        {
            //The business work flow has been completed.
            this.bus.Publish<ProgressReportedEvent>(p => p.Description = string.Format("Accommodation {0} has been succesfully published", message.Data.Name));
        }
    }
}
