﻿using Newtonsoft.Json;
using PersistenceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            AddPrices();
            GetAll();

            Console.ReadLine();
        }

        public static void AddPrices()
        {
            PersistenceLayer.Repository repo = new PersistenceLayer.Repository();

            AccommodationWriteModel accommodation01 = new AccommodationWriteModel();
            accommodation01.Name = "Residence Les Chalets du Thabor";
            accommodation01.Prices.Add(new PriceWriteModel() { PurchasePrice = 200, SellingPrice = 250, ArrivalDate = DateTime.Parse("02/01/16") });
            accommodation01.Prices.Add(new PriceWriteModel() { PurchasePrice = 100, SellingPrice = 125, ArrivalDate = DateTime.Parse("09/01/16") });
            accommodation01.Prices.Add(new PriceWriteModel() { PurchasePrice = 150, SellingPrice = 180, ArrivalDate = DateTime.Parse("16/01/16") });

            AccommodationWriteModel accommodation02 = new AccommodationWriteModel();
            accommodation02.Name = "Hotel Olympia";
            accommodation02.Prices.Add(new PriceWriteModel() { PurchasePrice = 350, SellingPrice = 425, ArrivalDate = DateTime.Parse("02/01/16") });
            accommodation02.Prices.Add(new PriceWriteModel() { PurchasePrice = 300, SellingPrice = 350, ArrivalDate = DateTime.Parse("07/01/16") });
            accommodation02.Prices.Add(new PriceWriteModel() { PurchasePrice = 320, SellingPrice = 380, ArrivalDate = DateTime.Parse("13/01/16") });

            AccommodationWriteModel accommodation03 = new AccommodationWriteModel();
            accommodation03.Name = "Les Chalets de SuperDévoluy";
            accommodation03.Prices.Add(new PriceWriteModel() { PurchasePrice = 180, SellingPrice = 200, ArrivalDate = DateTime.Parse("02/01/16") });
            accommodation03.Prices.Add(new PriceWriteModel() { PurchasePrice = 90, SellingPrice = 120, ArrivalDate = DateTime.Parse("09/01/16") });
            accommodation03.Prices.Add(new PriceWriteModel() { PurchasePrice = 90, SellingPrice = 130, ArrivalDate = DateTime.Parse("18/01/16") });

            repo.Save(accommodation01,accommodation02,accommodation03);
        }

        public static void GetAll()
        {
            PersistenceLayer.Repository repo = new PersistenceLayer.Repository();
            foreach (AccommodationReadModel accommodation in repo.All())
            {
                Console.WriteLine("Name: {0}", accommodation.Name);
                foreach (var price in accommodation.Prices)
                {
                    Console.WriteLine("\tDate: {0} | Selling price: {1}", price.ArrivalDate, price.SellingPrice);
                }
            }
        }
    }
}
