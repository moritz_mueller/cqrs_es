﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenceLayer
{
    class PriceMapping : ClassMap<Price>
    {
        public PriceMapping()
        {
            this.Id(e => e.Id);
            this.Map(e => e.PurchasePrice);
            this.Map(e => e.SellingPrice);
            this.Map(e => e.ArrivalDate);
        }
    }
}
