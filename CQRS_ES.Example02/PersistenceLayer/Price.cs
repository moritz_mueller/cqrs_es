﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenceLayer
{
    public class Price
    {

        public virtual int Id
        {
            get;
            protected set;
        }

        public virtual decimal PurchasePrice
        {
            get;
            set;
        }

        public virtual decimal SellingPrice
        {
            get;
            set;
        }

        public virtual DateTime ArrivalDate
        {
            get;
            set;
        }
    }
}
