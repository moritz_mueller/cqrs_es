﻿using FluentNHibernate.Mapping;

namespace PersistenceLayer
{
    class PriceReadModelMapping : ClassMap<PriceReadModel>
    {
        public PriceReadModelMapping()
        {
            this.Id(e => e.Id);
            this.Map(e => e.SellingPrice);
            this.Map(e => e.ArrivalDate);
            this.Table("Price");
        }
    }
}
